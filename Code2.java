//{ Driver Code Starts
// Initial Template for Java

import java.io.*;
import java.util.*;
class GFG {
    public static void main(String[] args) {

        Scanner sc = new Scanner(System.in);
        
	int t = sc.nextInt();

        while (t-- > 0) {
            int n = sc.nextInt();

            int arr[] = new int[n];

            for (int i = 0; i < n; i++) {
                arr[i] = sc.nextInt();
            }
            Solution ob = new Solution();
            System.out.println(ob.firstRepeated(arr, n));
        }
    }
}

class Solution {
    public static int firstRepeated(int[] arr, int n) {
        HashMap<Integer, Integer> indexMap = new HashMap<>();
        int minIndex = Integer.MAX_VALUE;
        
        for(int i = n - 1; i >= 0; i--){
            int currentElement = arr[i];
            
            if(indexMap.containsKey(currentElement)){
                minIndex = i + 1;
            } else{
                indexMap.put(currentElement, i + 1);
            }
        }
        return (minIndex == Integer.MAX_VALUE) ? -1 : minIndex;
    }
}
