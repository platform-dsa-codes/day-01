import java.util.*;

class Solution {
    public int singleNumber(int[] nums) {
        int result = 0;
        for(int num : nums){
            result ^= num;
        }
        return result;
    }

    public static void main(String[] s){
        Solution sol = new Solution();
        Scanner sc = new Scanner(System.in);

        System.out.print("Enter the number of elements in the array:");
        int n = sc.nextInt();
        int nums[] = new int[n];

        System.out.println("Enter the elements of the array:");
        for(int i = 0; i < n; i++){
            nums[i] = sc.nextInt();
        }

        int result = sol.singleNumber(nums);
        System.out.println("The single number is: " + result);
    }
}
